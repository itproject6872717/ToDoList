﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace ToDoList
{
    public class HTTPReq_CL
    {
        public static readonly HttpClient client = new HttpClient();
        public async void LogIn(string logIn, string password)
        {
            var values = new Dictionary<string, string>
              {
                  { "login", $"{logIn}" },
                  { "password", $"{password}" }
              };

            var content = new FormUrlEncodedContent(values);

            var response = await client.PostAsync("http://localhost:3000/login", content);

            var responseString = await response.Content.ReadAsStringAsync();
            JSONResponse js = JsonConvert.DeserializeObject<JSONResponse>(responseString);
            MessageBox.Show(js.status.ToString());
        }
    }
    class JSONResponse
    {
        public string status { get; set; }
    }
}
