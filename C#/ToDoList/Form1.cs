﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Http;
using Newtonsoft.Json;

namespace ToDoList
{
    public partial class LogIn : Form
    {
        string temp_username = "";
        string temp_password = "";

        bool full_width = false;
        public static readonly HttpClient client = new HttpClient();
        class JSONResponse
        {
            public string status { get; set; }
        }

        public LogIn()
        {
            InitializeComponent();
        }

        private async void LogInBtn_Click(object sender, EventArgs e) // Логін
        {
            string log = LogInField.Text;
            string pass = PasswordField.Text;
            var values = new Dictionary<string, string>
              {
                  { "username", log },
                  { "password", pass }
              };

            var content = new FormUrlEncodedContent(values);

            var response = await client.PostAsync("http://localhost:3000/login", content);

            var responseString = await response.Content.ReadAsStringAsync();
            JSONResponse js = JsonConvert.DeserializeObject<JSONResponse>(responseString);
            if(js.status.ToString() == "Access accepted")
            {
                temp_username = LogInField.Text;
                temp_password = PasswordField.Text;
                MenuPanel.Visible = true;
                login_panel.Visible = false;
                list_panel.Visible = true;
            }
            else if(js.status.ToString() == "No such user")
            {
                log_help_label.Text = "*Такого користувача не існує";
            }
            else if (js.status.ToString() == "Wrong password")
            {
                log_help_label.Text = "*Невірний пароль";
            }
        }

        private void button1_Click(object sender, EventArgs e) // Мінімізувати вікно
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void LogIn_Load(object sender, EventArgs e) // При завантаженні форми, сховати границю
        {
            this.FormBorderStyle = FormBorderStyle.None;
            list_panel.Visible = false;
            login_panel.Visible = false;
            register_panel.Visible = true;
            MenuPanel.Visible= false;
        }

        private void button3_Click(object sender, EventArgs e) // Закрити вікно
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e) // На весь екран вікно
        {
            if (!full_width)
            {
                this.WindowState = FormWindowState.Maximized;
                full_width= true;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                full_width = false;
            }
        }

        private void log_to_reg_link_Click(object sender, EventArgs e) // Перенаправлення на форму реєстрування
        {
            register_panel.Visible = true;
            login_panel.Visible = false;
        }

        private void reg_to_log_link_Click(object sender, EventArgs e)// Перенаправлення на форму логіну
        {
            register_panel.Visible = false;
            login_panel.Visible = true;
        }

        private async void reg_btn_Click(object sender, EventArgs e) // Реєстрація
        {
            string reg_name = registerField.Text;
            string pass = pass_reg_field.Text;
            if (reg_name.Length >= 4 & pass.Length >= 6)
            {
                var values = new Dictionary<string, string>
              {
                  { "username", reg_name },
                  { "password", pass }
              };

                var content = new FormUrlEncodedContent(values);

                var response = await client.PostAsync("http://localhost:3000/register", content);

                var responseString = await response.Content.ReadAsStringAsync();
                JSONResponse js = JsonConvert.DeserializeObject<JSONResponse>(responseString);
                if(js.status.ToString() == "Such user already exists")
                {
                    reg_help_label.Text = ("*Користувач з таким іменем вже зареєстрований");
                }else if(js.status.ToString() == "User created")
                {
                    login_panel.Visible=true;
                    register_panel.Visible=false;
                }
            }
            else
            {
                reg_help_label.Text = "*Ім'я користувача повинно бути >= 4 символів, а пароль >= 6";
            }
        }

        private void log_out_btn_Click(object sender, EventArgs e)
        {
            MenuPanel.Visible = false;
            login_panel.Visible = true;
            temp_username = "";
            temp_password = "";
            list_panel.Visible = false;
        }

        private void create_btn_Click(object sender, EventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {

        }
    }
}
